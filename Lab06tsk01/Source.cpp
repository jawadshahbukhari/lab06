#include <iostream>
#include<cstdlib>
#include <vector>
#include <time.h>
#include <algorithm>
using namespace std;

/*bool moveMin(vector<int> &in, vector<int> & out) {

	int start_s = clock();
	out = in;
	for (int i = 0; i < out.size() ; i++) {
		for (int j = i + 1; j < out.size() ; j++) {

			if (out[i] > out[j]) { swap(out[i], out[j]); }

		}

	}
	int stop_s = clock();
	int t = (stop_s - start_s) / double(CLOCKS_PER_SEC) * 1000;
	cout << "time in milli seconds for single loop: " << t << endl;

	return true;

}*/

bool moveMin(vector<int> &in, vector<int> & out) {

	int start_s = clock();
	out = in;
	for (int i = 0, j = i + 1; i < out.size() && j < out.size();)
	{
		if (out[i] > out[j])
		{
			swap(out[i], out[j]);
			i = 0;
			j = i + 1;
		}
		else
		{
			i++;
			j++;
		}
	}
	int stop_s = clock();
	int t = (stop_s - start_s) / double(CLOCKS_PER_SEC) * 1000;
	cout << "time in milli seconds for single loop: " << t << endl;
	return true;

}



bool testMoveMin(vector<int> &in, vector<int> & out) {



	for (int i = 0; i < in.size(); i++) {

		if (in[i] != out[i]) {

			return false;

		}




	}


	return true;


}

int main() {

	

	srand(time(NULL));
	vector <int> in, out;
	for (int i = 150; i >0; i--) {

		in.push_back(i);
	}

	moveMin(in, out);

	//INPUT ARRAY
	cout << "INPUT ARRAY" << endl;
	for (int i = 0; i < in.size(); i++) {

		cout << in[i] << ",";

	}
	cout << endl;

	//MY SORT
	cout << "MY SORTED ARRAY" << endl;
	for (int i = 0; i < in.size(); i++) {

		cout << out[i] << ",";

	}
	cout << endl;

	cout << "built-in SORTED ARRAY" << endl;
	sort(in.begin(), in.end());
	for (int i = 0; i < in.size(); i++) {

		cout << in[i] << ",";

	}
	cout << endl;

	if (testMoveMin(in, out))
		cout << "your algo works :)" << endl;
	else
		cout << "Oops! your test algo doesnt work right :(" << endl;

	system("pause");
}